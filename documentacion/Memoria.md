# PROYECTO DE CICLO DE G.S DE 
**Por Alberto Merlo Tejero**

![Logo del instituto IES Virgen Del carmen](images/logoies.png)


# Automatización de tareas mediante Ansible

# RESUMEN
Este proyecto se basa en la tecnología de [ansible](https://www.ansible.com) para automatizar acciones a uno o varios clientes.

En los que podremos ejecutar un siemple comando como "free -h" para ver los datos de la memoria ram, hasta crear despliegues completos de servidores como por ejemplo un servidor "NextCloud" totalmente funcional con un simple intro.

La filosofía de ansible es simplificar lo máximo posible las acciones que tiene que realizar un administrador de tareas, evitando así los posibles errores humanos al mandar todo el rato el mismo comando, o incluso eliminar la redundancia de acciones como puede ser reiniciar más veces de las necesarias un servicio.

A lo largo del proyecto, veremos cómo podemos realizar acciones sencillas y muy complejas.

También se verá de manera algo "superficial" la tecnología docker.

Aunque el proyecto se centrará principalmente en la vista general de la tecnología de ansible y en mostrar varios ejemplos de uso sobre como llevar a cabo ciertas tareas rutinarias para un administrador de tareas.

Se hará una introducción al perfil de DevOps y su función en una empresa, dado que Ansible es una herramienta muy utilizada por dicho perfil.

*Me gustaría aclarar que este documento ha sido generado enteramente en Markdown, por lo que si está leyendo una versión en papel, PDF, Word... o cualquier formato que haga uso de la división por páginas, la imposibilidad de seleccionar texto o hacer click en algún enlace, le recomiendo encarecidamente que use la versión en markdown o web. La versión en MarkDown está disponible para su lectura online en el repositorio de GitLab, junto con todo el proyecto*

**Link hacia la memoria en Markdown en GitLab:** [https://goo.gl/FpQ4bf](https://goo.gl/FpQ4bf)

![](images/qr-md-mini.jpg)

# INTRODUCCIÓN
## ¿Qué es ansible?

![](images/ansible.png)

Ansible es una herramienta que nos permite automatizar tareas repetitivas como por ejemplo:

- Hacer configuraciones completas de un equipo.
- Obtener datos de todos los ordenadores de un aula.
- Desplegar servidores.
- Administrar usuarios de muchos PCs a la vez.

Y todo esto se realiza con un simple comando de consola, ésto significa, que los clientes no necesitarán ningún tipo de agente para funcionar

## ¿Por qué ansible?

Existen ya varios métodos de automatizar las acciones en los equipos, como [puppet](https://puppet.com/), pero estos necesitan de agentes para realizar esas acciones, mediante que con ansible simplemente es crear un archivo con las acciones que quieres que se realicen y lanzarlo al cliente.

Esto nos asegura que no habrá problemas al lanzar las acciones porque el agente no esté instalado, no funcione, no sea compatible con esa versión del sistema operativo... etc.

Una de las características fundamentales de ansible es la **idempotencia**. La idempotencia en este caso hace referencia a que si por ejemplo, ejecutamos la acción de instalar "htop", pero ya está instalado en el erquipo, no se volverá a instalar. Es una gran ventaja ya que nos ahorraremos muchísimos problemas de pérdidas de configuraciones al volver a instalar de nuevo el software, además de, por supuesto, mucho tiempo al no realizarse acciones que no son necesarias.

Tiene un montón de módulos que nos permiten realizar cambios en el sistema independientemente de dónde lo estemos lanzando, pues él mismo reconoce el sistema operativo anfitrión y ejecuta con las instrucciones que se le ha dado unos u otros comandos propios del sistema operativo.

Los clientes pueden ser multiplataforma, es decir, puedes ejecutar acciones tanto en Linux, como en Windows como en Mac OS.

Esto simplifica mucho la tarea del administrador de sistemas, ya que en vez de tener que conectarnos individualmente a todos los servidores o sistemas para lanzar esas acciones, solo habrá que ejecutar un comando e irse a tomar un café mientras ansible se ocupa de todo.

### ¿Cómo funciona ansible?

Ansible se conecta a todos los clientes mediante SSH y les envía a todos la misma instrucción. Es como conectarse tú mismo mediante SSH cliente por cliente, pero a todos a la vez y realizar acciones de forma simultánea y automática.

![](images/esquema1.png)

Lo único que tendremos que tener en cuenta es que los clientes deben tener instalado python2 (no sirve python3).

### ¿Cómo instalar ansible?

Como se mencionó anteriormente ansible se conecta a los clientes mediante SSH y no es necesario un agente en los clientes, por lo que solo hay que instalar ansible en la máquina desde la que se lanzarán los comandos

Instalamos los softwares de terceros si no lo tenemos:

`$ sudo apt install software-properties-common`

Ahora agregamos el repositorio oficial de ansible:

`$ sudo apt-add-repository ppa:ansible/ansible`

Actualizamos los repositorios:

`$ sudo apt update`



Instalar ansible:

`sudo apt install ansible`

### ¿De qué partes está compuesto Ansible?

```
├── hosts <- inventario de clientes
├── playbook.yml <-playbook
└── roles
    └── webserver <- Rol con nombre: webserver
        ├── files
        │   └── index.php
        └── tasks
            └── main.yml <- fichero de tareas del rol "webserver"
```


### ¿Cómo puede trabajar ansible?

Ansible, por defecto, tiene un inventario de direcciones (vacío) llamado "hosts" ubicado en la dirección:

`$ sudo nano /etc/ansible/hosts` (para linux)

`$ sudo nano /usr/local/etc/ansible/hosts` (para mac)

En él, se guardan todas las direcciones organizadas por grupos y los datos de acceso a los mismos, aquí un ejemplo de su contenido:

![](images/img3.png)

- **[\<nombre-grupo>]**: Esto hace referencia al grupo que queramos crear en ansible y que luego llamaremos en los comandos por ese grupo.

- **[\<nombre-grupo>:vars]**: Esto define las variables que queremos que se apliquen a todo el grupo.

- **ansible_user**: Sustituye a **-u \<usuario-ssh>**, que equivale al usuario ssh que se usará en todos los clientes del grupo "clase17".

- **ansible_ssh\_pass**: Sustituye a **--ask-pass** que es la contraseña del ssh que, de nuevo, se usará en todos los clientes del grupo. "clase17".

Una vez que se introducen los datos de los clientes en el archivo "hosts", podremos ejecutar el siguiente comando para mandar una orden a todos ellos:


`ansible clase17 -a "free -h"` (free -h nos da los datos de la memoria ram en formato humanizado "-h")

![](images/img4.png)



También puede recoger las direcciones de los clientes directamente desde el comando con:

`ansible all -i <IP-cliente>, -a "uname -a" -k -u cibertos`

- **all:** Necesario para indicar a todos los clientes. (al llevar el "-i", all hace referencia a todos los clientes de "-i", si no llevara el "-i", actuaría indicanto a todos los grupos del archvo "hosts)
- **-i:** Parámetro para indicar los clientes a los que queramos enviar el comando. (la coma final es indispensable)
- **-k:** Parámetro para indicar la clave SSH del servidor. (no  se debe de poner en el comando la contraseña, nos lo preguntará después)
- **-u:** Parámetro para indicar el usuario SSH del servidor.


### ¿Cómo ejecutar tareas complejas?

En los ejemplos anteriores había comandos sencillos que nos mostraban como recoger la información del sistema, o la memoria libre del mismo, pero, ¿y si quiero realizar tareas más complejas?

Para ello están los **ROLES** y los **PLAYBOOKS**, ambos, funcionan mediante la extructura de archivos [yamel](yamel) y realizan las acciones requeridas mediante tareas que llaman a módulos de ansible.

### Módulos:

Los módulos son las unidades de trabajo de Ansible, están escritos en python, perl, bash... etc.

La ventaja de usar módulos y no comandos del sistema, esque los módulos están hechos para trabajar y realizar las acciones correctamente independientemente del sistema linux. CentOS, Debian, Ubuntu... etc


### Tasks (tareas):

Las tasks o tareas, son las acciones individuales que realizará ansible, están compuestas por un módulo.



### Playbooks:
Los playbooks son ficheros unitarios que inicialmente no dependen de ningún otro archivo para su ejecución, pueden contener cualquier instrucción de cualquier módulo de ansible dentro de ellos.

![](images/esquema2.png)


Aquí un ejemplo sencillo: *install-uninstall-nmap.yml*

```yaml
- hosts: " clase17"

  # Necesario para lanzar comandos root, como "apt"
  become: yes

  tasks:

   # instalamos el comando "nmap"
    - name: Instalar nmap.
      apt: # indicamos que usamos el módulo apt
        name: nmap # nombre del paquete a instalar

   # Comprobación de funcionamiento de nmap y lo guardamos en una variable.
    - name: Mostrar versión de nmap
      shell: nmap -V
      register: variable1

   # Pintamos la variable:
    - debug: msg="{{ variable1.stdout }}"

   # Desinstalamos nmap
    - name: Desinstalar nmap
      apt:
        name: nmap
        state: absent # Indica el estado que queremos tener del paquete.
```

Ejecutamos el playbook con el comando:

`ansible-playbook <playbook>.yml --ask-sudo-pass`

- **<\\playbook>.yml** hace referencia una ruta. Ej: *Escriorio/nombre-playbook.yml*
- **"--ask-sudo-pass"** equivale a "**-K**"

![](images/img5.png)

### Roles

Los roles, son un conjunto de archivos que dependen unos de otros para realizar acciones. Al uso, se podría decir que son playbooks separados por temáticas. Esta es la estructura básica de un rol que se puede generar de manera automática con el comando:

`ansible-galaxy init <nombre-del-rol>`

 ![](images/img6.png)

- **Defaults:** Esta carpeta contiene los archivos con las variables globales que se usarán en el rol durante sue ejecución. Por ejemplo, puede contener el puerto de escucha que usará nextcloud, o la ruta de su instalación.

- **Handlers:** Esta carpeta contiene los archivos con los que se controlarán ciertas acciones de cada bloque de tareas (tasks) y así asegurar que las tareas acaben en el estado deseado. Se utiliza la etiqueta "notify" al final de la task que queramos controlar.

 Por ejemplo, si al instalar apache realizamos ciertas acciones que requieren su reinicio  (habilitar un sitio y deshabilitar otro) apache, en vez de reiniciarse dos veces, se reiniciaría una, evitando así reinicios innecesarios.

- **Meta:** En estos archivos se especifica el entorno, SO, versión de software, autor, licencia... etc

- **Tasks:** Aquí se especifican todas las tareas que requiere el rol.

- **Test:** Aquí irá el código que se encarga de comprobar que todo está corréctamente formado.

- **Vars:** En este directorio solo se diferencia del directorio "defaults" por dos motivos:

 - Las variables definidas en estos archivos tienen más prioridad que las definidas en la carpeta "defaults"
 - Las variables aquí definidas se pueden llamar desde distintos archivos del rol.

No es necesario que un rol contena todas las partes descritas anteriormente, un rol podría ser solo la carpeta defaults y la carpeta task, o incluso solo task.


## ¿Qué es Docker?

![](images/docker.png)

A lo largo de este trabajo también se usará [docker](https://www.docker.com/) para realizar algunos ejemplos.

Docker no es un sistema de virtualización, aunque en sus primeras versiones si lo era (virtualización ligera). Esto quiere decir, que a groso modo, es parecido a una máquina virtual, pero con unas diferencias bastante considerables.

Las máquinas virtuales requieren de un hypervisor para emular el sistema operativo al completo. Mientras que Docker, lo que realiza es la virtualización del sistema **usando el kernel del SO anfitrión**.

Con Docker, creas contenedores a partir de una imagen, éstos contenedores serían el simil a

_Como es lógico, si se hace uso de la tecnología docker en un entorno Windows, el kernel de linux estaría virtualizado, por lo que lo más probable es que no vaya tan bien como si se realiza en un entorno linux._

![](images/docker1.png)

##¿Por qué Docker?

Entre las múltiples ventajas de Docker, se encuentra la **portabilidad**, **ligereza** y la **Autosuficiencia**

### Portabilidad:

La principal ventaja de Docker, es su portabilidad. Al crear contenedores totalmente aislados, podemos moverlo a cualquier equipo (que soporte docker, claro), que esta seguirá funcionando exactamente igual que lo hacía en la máquina donde se creó.

Esto facilita mucho las tareas de despliegue en una empresa, ya que si un contenedor funciona en el entorno de pre-producción, al moverlo a producción nos aseguramos de que vamos a obtener el mismo resultado.

### Ligereza:
Como ya se comentó hace unos párrafos, Docker no es un SO emulado desde cero, si no que solo contiene las librerías y aplicaciones necesarias para ejecutar la aplicación que nosotros queramos que tenga.

Por ejemplo, si queremos realizar una instalación de apache + una web, lo más seguro es que si virtualizamos con virtualbox una máquina desde cero y únicamente instalamos esos dos servicios, la máquina ocupará como poco 1 GB.

Mientras que si utilizamos docker, al solo requerir los archivos necesarios para el funcionamiento de apache + la página web, con Docker ese mismo escenario ocuparía alrededor de 200 MB, ya que hace uso del kernel del sistema anfitrión.


### Entorno por capas:

Otra de las grandes ventajas que ofrece Docker frente a las máquinas virtuales es su estructura formada por capas.

![](images/docker2.jpg)


En la imagen anterior, está mostrado el sistema de capas de docker, esto quiere decir que:

Si creamos un contenedor con CentOS7 que ocupa 100MB, el resultado será este:

- Contenedor con CentOS7

**Espacio ocupado por Docker y sus contenedores:** 100 MB

Ahora, decidimos que queremos, un contenedor aparte que además de CentOS7 tenga instalado Node JS que ocupa 20MB, el espacio en disco que tendremos será:
```
- Contenedor con CentOS7
- Contenedor con CentOS7 y NodeJS 6.9

**Espacio ocupado por Docker y sus contenedores:** 120 MB
```
Como se aprecia, la capa de CentOS7 se reutiliza, por lo que en nuestro disco duro solo ocupará espacio una vez (que se reutilice una capa no implica que los datos se mezclen de un contenedor a otro)


# Objetivos

El objetivo de este proyecto es investigar sobre el software de gestión "Ansible" y sus mútliples aplicaciones y ventajas sobre el entorno de un administrador de sistemas.

Asimismo también se verán ejemplos con otras tecnologías de distinta índole; En el que se hará uso de Docker

## Objetivo secundario

### Markdown

Este proyecto ha sido generado íntegramente en markdown, por lo que ciertamente, he tenido algunas limitaciones a la hora de dar formato al documento tal y como estaba descrito en las normas del instituto. 

Aunque ha quedado muy parecido a lo requerido, el único impedimento ha sido que no he podido poner una imagen de portada en la primera página, por lo que he tenido que agregarla después de generar el PDF.

Markdown es un lenguaje de marcado ligero, con el que podremos poner las letras en negrita, cursiva... etc con los propios símbolos del lenguaje como los asteriscos o los guiones bajos.

A partir de ese documento de markdown, se puede exportar a múltiples formatos, como PDF, que es el caso a tratar.

# Hipótesis

Lo que se pretende con este trabajo más allá de ver y conocer Ansible, es de iniciarse como administrador de sistemas en el mundo de los DevOps que tanta demanda tiene actualmente.

La palabra **DevOps** es un acrónimo en inglés basado en Development (desarrollo) y Operations (Operaciones).

Este perfil es el de un trabajador que está un poco entre estos dos entornos, el de desarrollo, y el de operaciones.

Sus funciones son las de automatizar mediante lenguajes y software el despliegue del software que se desarrolle en el departamento de desarrollo y la creación y programación de las herramientas y plantillas necesarias que se realiza en la integración contínua, asegurandi así la calidad de los despliegues que se realicen del proyecto que está creando en el departamento de desarrollo.

![](images/devops.png)


# Material

Para la realización de la práctica se hizo uso de:

**Hardware:**

- Un ordenador (con sistema operativo Mac OS).

**Software:**

- Ansible: `2.3.0.0`
- MacOS: `10.12.5`
- Ubuntu: `16.04 (amd64)`
- Tecnología SSH.
- Docker `17.03.0-ce`


# Método

En el apartado de introducción se explicó como funcionaban las tecnologías que utilizan en este trabajo.

Antes de empezar a realizar acciones, también se puede puede automatizar el acceso mediante la clave SSH.

## Configuración de la clave ssh.

Ansible se conecta a los clientes mediante ssh, por lo que también podremos enviar nuestra clave ssh a los servidores para que no nos pidan contraseña ssh (no sudo):


Crear la clave SSH: (si no está ya generada)

`$ mkdir ~/.ssh ` (si no está creado)

`$ cd ~/usuario/.ssh`

Copiar la clave SSH al cliente ansible:

`ssh-copy-id -i id_rsa.pub usuario@ipcliente`

## ¿Qué se puede realizar con ansible de forma sencilla?

### Obtener el espacio libre de un cliente determinado:

**Con el siguiente comando:**
`$ ansible all -i <ip>, -a "df -h" -u <usuario-ssh> --ask-pass `

 ![](images/img2.png)

### Obtener el espacio libre en múltiples clientes:
`$ ansible all -i <ip>,<ip2>, -a "df -h" -u <usuario-ssh> --ask-pass `

![](images/img1.png)

### Obtener el espacio libre de un grupo:

Para crear un grupo, primero hay que editar el archivo hosts de ansible:



**Lanzamos el comando para el grupo "clase17"**:

El comando se ha simplificado bastante respecto a la anterior vez:



## ¿Qué se puede hacer con ansible de manera avanzada?

Ansible, como se comentó al principio del documento, dispone de roles y playbooks.


###Ejemplo práctico de lo que sería un rol sencillo:

Se llegará al mismo fin que en el ejemplo del playbook, pero esta vez, en un rol.

Solo tendremos una carpeta (ya se indicó que no es necesario crear todas las carpetas que pueden existir en un rol para crearlo.

La estructura de carpetas será la siguiente:

![](images/img8.png)

**La categoría de "tasks", la pasaremos al archivo tasks/main.yml**

y se quedara así:

```yaml
# instalamos el comando "nmap"
- name: Instalar nmap.
  apt: # indicamos que usamos el módulo apt
    name: nmap # nombre del paquete a instalar

# Comprobación de funcionamiento de nmap y lo guardamos en una variable.
- name: Mostrar versión de nmap
  shell: nmap -V
  register: variable1

# Pintamos la variable:
- debug: msg="{{ variable1.stdout }}"

# Desinstalamos nmap
- name: Desinstalar nmap
  apt:
    name: nmap
    state: absent # Indica el estado que queremos tener del paquete.
```
Con este archivo editado ya está el rol listo para funcionar, lo único que faltaría sería lanzarlo mediante un playbook.

Se creará un **instalador.yml** donde se especificarán los host y los roles que se ejecutarán.

El directorio donde estará **instalador.yml** en la raíz de la carpeta donde esté ubicado el rol (para evitar poner rutas)

![](images/img9.png)

Al tener ya las task en el rol, obviamente prescindiremos de ellas en el playbook, y solo tendremos que llamar al ROL con el módulo "rol":

```yaml
- hosts: "*"
  roles:
    - install-uninstall-nmap #Nombre del rol, hace referencia a la carpeta.
  become: yes #Con esto se permite el uso de sudo.

```

Ahora para lanzar el playbook se repiten los pasos de la última vez pero cambiando el nombre del playbook:

`ansible-playbook -i <ip-servidor>, instalador.yml -u cibertos -k -K`

**-k:** Nos pedirá la clave SSH
**-K:** Nos pedirá la clave para hacer sudo.

![](images/img10.png)

### Ejemplo de rol haciendo uso de variables:

Una de las cosas más importantes en los despliegues de servidores son las variables. No todos los servidores son iguales, tienen distintas direcciones, distintas claves... etc.

Las variables se declaran o en el archivo **defaults/main.yml** o en el archivo **vars/main.yml** teniendo este último más prioridad sobre las variables.

La estructura de este rol será la siguiente:

![](images/img11.png)

En **defaults/main.yml** se declaran las variables, con la estructura **`clave: "valor"`**

```yaml
---
# Variables que usaremos en el comando del programa
pass_number: "16"
long_pass: "5"
```

En **vars/main.yml** se declara otra variable, que hará referencia al nombre del programa que vamos a instalar, con la misma esttructura **`clave: "valor"`**

```yaml
# Declaramos la variable para instalar el programa
programa: "pwgen"
```
Por último en **tasks/main.yml** pondremos la instalación, la ejecución y la desinstalación del programa. Para introducir las variables tendrán que ir entre doble llave **`{{ variable1 }}`**

```yaml
- name: Instalar "{{ programa }}".
  apt:
    name: "{{ programa }}"

- name: Usar "{{ programa }}"
  shell: pwgen -N "{{ pass_number }}" "{{ long_pass }}"
  register: variable1

- debug: msg="{{ variable1.stdout }}"

- name: Desinstalar "{{ programa }}"
  apt:
    name: "{{ programa }}"
    state: absent
```

Por último, el playbook para ejecutar el rol es prácticamente idéntico al anterior, pero cambiando el nombre del rol:

```yaml
- hosts: "*"
  roles:
    - install-uninstall-programa-variables #Nombre del rol, hace referencia a la carpeta.
  become: yes #Con esto se permite el uso de sudo.
```

El resultado es este:

`ansible-playbook -i <ip-servdior>, install-uninstall-programa-variables.yml -u cibertos -k -K`

![](images/img12.png)

*Como se aprecia en la salida del módulo "debug", aparecen las contraseñas del programa "pwgen" separadas por "\"*

### Ejemplo de rol usando el módulo templates:

Una de las mayores ventajas que tiene es el módulo templates. Este módulo nos pemite copiar un archivo de de texto (.txt, .php, .json ...etc) en el servidor, pero con la ventaja de que admite variables, y éstas se sustituirán por las que tengamos nosotros declaradas en **`defaults/main.yml`**

El módulo templates hace uso del lenguaje "jinja2", que es un lenguaje de creación de plantillas para python.


Aquí un ejemplo sencillo en el que enviaremos un fichero con unas variables y luego lo mostraremos por consola.

Estructura:
![](images/img13.png)

Esta vez haremos uso de las carpetas **defaults**, **tasks** y **templates**

**`defaults/main.yml`**

```yaml
---
titulo_pagina: "Ejemplo con templates"
contenido_pagina: |
  <div>
  <p>Esto es un ejemplo de página HTML generada mediante una plantilla en ansible.</p>
  <p>El caracter "|" que está a continuación de la variable
  indica que es una variable multilínea.</p>
  <p>Así podremos poner un bloque entero de texto en una única variable.</p>
  </div>
```

**`tasks/main.yml`**

```yaml
- name: Copiar index.html
  template:
    src: index_prueba.html.j2 #no es necesario indicar el directorio "templates"
    dest: /tmp/index.html #el nombre del destino lo decidimos nosotros.

- name: Mostrar contenido "index.html"
  shell: cat /tmp/index.html
  register: variable1

- debug: msg="{{ variable1.stdout }}"

```

**`templates/index_prueba.html.j2`**

```html
<!DOCTYPE html>
 <html>
 	 <head>
  		 <meta charset="UTF-8">
		 <title>{{ titulo_pagina }}\</title>
	 </head>

	 <body>
		 {{ contenido_pagina }}
	 </body>

 </html>

```
Playbook: **`rol-template.yml`**

```yaml
- hosts: "*"
  roles:
    - rol-template
# No es necesario el "become" ya que en /tmp puede escribir todo el mundo
```

Resultado:


![](images/img13.png)

*Recalcar que el HTML mostrado en el paso "debug" no se muestra correctamente formateado por temas de ansible, pero el documento original está correcto.*

![](images/img15.png)


### Ejemplo de rol usando el módulo user y group

Otra módulo muy útil es el módulo de "user" que se encarga de administrar usuarios.
En este ejemplo se crearán dos usuarios y un grupo mediante el módulo group.

La estructura será la siguiente:


**`defaults/main.yml`**

```yaml
---
user: "usuarioderelleno"
pass: "stallman"

group: "grupoderelleno"

```

**`tasks/main.yml`**

```yaml
- name: Añadir grupo "{{ group }}"
  group:
    name: "{{ group }}"

- name: Agregar usuario {{ user }}
  user:
    name: "{{ user }}" #nombre del usuario
    password: "{{ pass }}" #contraseña que tendrá el usuario

- name: Agregar {{ user }} a los grupos
  user:
    name: "{{ user }}"
    groups:
      - "{{ user }}"
      - adm
      - "{{group}}"

```
*Es necesario crear el usuario y luego agregarlo a los grupos, ya que si lo agregamos al crear el usuario nos dará un error ya que el grupo "usuariodeprueba" que se crea con el usuario no existe al momento de crearlo, si no a posteriori.*

**Muy importante** el diferenciar entre la opción **group** y la opción **groups**, el primero es para establecer el grupo primario y el segundo es para indicar a los grupos que pertenece el usuario.

Ninguna de esas dos opciones es obligatoria, pero si se añaden no pueden estar en blanco, o ansible hará lo que se le indicó y dejará sin grupo primario y lo sacará de todos los grupos, puesto que al dejerlo en blanco se le indicó que el usuario no tiene grupo primario ni pertenecea ningún grupo.

Playbook: **`crear-usuario-grupo.yml`**

```yaml
- hosts: "*"
  roles:
    - crear-usuario-grupo
  become: yes
```

Para ejecutarlo:
 `ansible-playbook -i <ip-servidor>, crear-usuario-grupo.yml  -u cibertos -k -K`

Resultado:

![](images/img16.png)

Se comprueba que todo está correctamente creado:

![](images/img17.png)

### Ejemplo de rol para controlar Docker.

Lo primero que hay que tener instalado en el servidor al que nos conectemos es docker.

Como ansible funciona mediante python, es imprescindible tener instalado el módulo docker para python, que se instalará al principio de las tasks.

Aprovechando que en un ejemplo anterior se creó un index.html en la carpeta "/tmp" reutilizaremos ese archivo para mostrarlo en el contenedor apache.

**`defaults/main.yml`**

```yaml
---
imagen: "eboraas/apache" # Nombre de la imagen que queremos usar para crear el contenedor 
etiqueta: "latest" #etiqueta de la imagen, normalmente hace referencia a la versión
```

**`tasks/main.yml`**

```yaml
- name: Instalar dependencias
  apt:
    name: python-docker

- name: Descargar imagen {{ imagen }}
# con esto se descarga la imagen necesaria para arrancar el contenedor
  docker_image:
    name: "{{ imagen }}"
    tag: "{{ etiqueta }}"


# Una vez que se descarga la imagen ahora toca arrancar el contenedor con ella.

- name: Arrancar contenedor con la imagen base descargada.
  docker_container:
    name: apache # Nombre identificativo para el contenedor
    image: "{{ imagen }}:{{ etiqueta }}" # Nombre de la imagen y su etiqueta
    ports:
      - "81:80" # <puerto-local>:<puerto-contenedor>
      - "444:443"
    volumes:
      - /tmp:/var/www/html #<directorio-local>:<directorio-contenedor>
```


Playbook: **`crear-usuario-grupo.yml`**

```yaml
- hosts: "*"
  roles:
    - crear-usuario-grupo
  become: yes
```

Para ejecutarlo:
 `ansible-playbook -i <ip-servidor>, docker-simple.yml  -u cibertos -k -K`

![](images/img18.png)

Una vez ejecutado y arrancado el contenedor, solo tendremos que poner la dirección del servidor y el puerto 81, ya que es ese el que se ha puesto para que redireccione al puerto 80 del contenedor.

![](images/img19.png)

### Envío de claves públicas a múltiples servidores

Uno de los mayores problemas que hay a la hora de administrar muchos servidores es no recordar la contraseña de todos, ya que, por seguridad, todos tendrían que tener una distinta y de la mayor longitud posible.


Para evitar recordar todas las contraseñas, se hará uso de una pareja de claves, una pública y para esto se deberá de enviar la clave pública que se quiera usar a los respectivos servidores.

También se usará el módulo **"user"**, ya que por norma general los servidores solo traen por defecto el usuario "root". (Servidores arrendados)

A destacar que el módulo "user" solo es necesario si el usuario no exite anteriormente en el sistema.

Se usará el módulo **"authorized_key"** para enviar las claves privadas.

Los servidores, al tener una contraseña distinta cada uno, ya no servirá el de ponerlo en el comando cunado nos lo pida el playbook, a si que se editará el archivo hosts.


update_password

Configuración del archivo **`ansible/hosts`**:

```yaml
[pruebassh]
192.168.1.218 ansible_user=cibertos ansible_ssh_pass=stallman ansible_become_pass=stallman
192.168.1.219 ansible_user=cibertos ansible_ssh_pass=stallman ansible_become_pass=stallman

```
*consultar los archviso digitales si algún trozo de código se corta.*

Archvo: **`tasks/main.yml`**

```yaml
---

# En el módulo User, usaremos la opción "update_password: on_create"
# por si acaso existe el usuario, no actualice su contraseña.
# Lo que hace esta opción es no actualizar la contraseña cuando lo crea
# la opción por defecto, si no se le indica nada es "always"
# que la actualizará siempre que se ejecute, aunque esté ya creado.

- name: Crear usuario Alberto
  user:
    name: alberto
    password: stallman
    update_password: on_create

- name: Clave usuario Alberto
  authorized_key:
    user: alberto
    key: "contenido de tuclave.pub"

```

Playbook: **`install-enviar-clave-ssh.yml`**

```yaml
---
- hosts: "*"
  roles:
    - enviar-clave-publica
  become:yes

```

Para ejecutarlo:
`ansible-playbook install-enviar-clave-ssh.yml`

![](images/img20.png)

### ¿Qué son los facts?

Los facts en ansible son las variables predefinidas que tienen los clientes, y estas variables guardan datos sobre la configuración e información del cliente.

Por ejemplo, en los facts, podremos saber que versión de sistema operativo tiene, la arquitectura, las tarjetas de red, que tarjeta de red es la primaria, el nombre del cliente, su IP, procesador, memoria ram, que kernel usa...etc.

Esas variables se autorellenan con los datos de cada servidor.

No todos los sistemas operativos tienen las mismas variables, por ejemplo, hay variaciones entre una versión de sistema operativo Debian y otra con Centos, aunque muchas de ellas las tengan en común.

Para saber las variables de un servidor en concreto se utiliza el módulo **setup**, este módulo no necesita de roles, ni playbooks, se puede utilizar mediante un simple comando, mostrándose en la terminal las variables anteriormente citadas.

Comando sin inventario:

`ansible all -i <ip1>, -m setup -u cibertos -k`

Comando con inventario: (se da por hecho que en el inventario están indicados el usuario ssh y su contraseña.

`ansible clase17 -m setup`

Esta es una muestra de las variables que se identifican con este comando:

```yaml
...
},
"ansible_distribution": "Ubuntu",
"ansible_distribution_major_version": "16",
"ansible_distribution_release": "xenial",
"ansible_distribution_version": "16.04",
"ansible_dns": {
    "nameservers": [
          "192.168.1.1"
          ]
    },
...

```

###Uso de la opción "When" y facts.

En muchas ocasiones tal vez solo interese realizar ciertas acciones del playbook cuando un servidor tenga una versión mayor de SO a 16.04, o enviar un archivo cuando sea de 64bits y otro distinto cuando sea de 32bits.

Para eso se puede usar **"when"**, que es una opción concicional. 

Se utiliza como en cualquier lenguaje de programación o pseudocódigo. When, utiliza las variables de entorno y las que nosotros definamos para funcionar. 

En este ejemplo se usará para decirle que cuando el cliente de ansible sea "cibertos" (nombre del equipo, no de usuario) cree el usuario "calamar" 

En este ejemplo se utilizará la variable llamada: **`ansible_hostname`**

**`tasks/main.yml`**

```yaml
---
- name: Crear usuario Calamar
  user:
    name: calamar
    password: stallman
  when: ansible_hostname == "cibertos"
```
Playbook: **`install-when-facts`**

```yaml
---
- hosts: "*"
  roles:
    - when-facts
  become: yes
```

Para ejecutarlo:

`ansible-playbook -i <ip>, -u cibertos install-when-facts.yml -k -K`

Resultado:

![](images/img21.png)

En caso contrario, si la comprobación diera negativo, esto es lo que se mostraría:

![](images/img22.png)

### Despliegue de un servidor de netxcloud
 
Como ya se vio anteriormente se pueden realizar grandes cosas con ansible, en este ejemplo se verá una instalación completa de un servidor nextcloud.

**Este ejemplo consta de 3 roles:**

- NextCloud (que incluye lo necesario para funcionar como apache o nginx)
- Mysql (para la base de datos de nextcloud)
- Dependencias (algunos repositorios necesarios y algún paquete adicional de python)

*Tanto el rol de NextCloud, como el rol de "Mysql" no han sido creados por mi, pero si han sido modificados para que funcionen sinérgicamente entre ellos para crear una instalación funcional desde el primer momento de nextcloud en un servidor de nueva instalación.*


**CONFIGURACIÓN DE NEXTCLOUD:** archivo **`nextcloud/defaults/main.yml`**

Aquí están las variables sobre los datos técnicos de instalación, que no es necesario cambiarlas, aunque algunas interesantes son las siguientes:

```yaml
#Host del servidor
nextcloud_db_host: "127.0.0.1"

#Tipo de base de datos que usará mysql
nextcloud_db_backend: "mysql"

#Forzar la instalación de https 
nextcloud_install_tls: true
nextcloud_tls_enforce: true
nextcloud_force_strong_apache_ssl: true

# Usuario y grupo dueños de los archivos
# Esto por supuesto es recomendable no tocarlo, 
# puesto que "www-data" es el usuario web de linux

# pero no está de más remarcarlo por si acaso se utiliza otro
nextcloud_websrv_user: "www-data"
nextcloud_websrv_group: "www-data"

```
**CONFIGURACIÓN DE MYSQL:** archivo  **`mysql/defaults/main.yml`**

En las variables de este archivo archivo tampoco son mucho más interesantes que cambiar salvo el puerto por defecto.


```yaml
# Puerto predeterminado para la instalación de la base de datos
mysql_port: '3306'

```

**VARIABLES PARA LA INSTALACIÓN:** archivo **`vars.yml`**

Aquí es donde están la mayor parte de las variables para la instalación del servidor:

```yaml

# [Mysql]
#nombre para el usuario root de la base de datos
mysql_root_user: "root"

#contrasea del usuario root
mysql_root_password: "stallman"


# [Nextcloud]
#nombre de la base de datos que se creará para nextcloud
nextcloud_db_name: "nextcloud"

#nombre del usuario y contraseña que 
#se creará para tener acceso a esa base de datos
nextcloud_db_admin: "ncadmin"
nextcloud_db_pwd: "stallman"

#nombre de usuario y contraseña que 
#tendrá el administrador de owncloud
nextcloud_admin_name: "admin"
nextcloud_admin_pwd: "stallman"


```

# Tiempo de ejecución

- **Investigación sobre la tecnología de Ansible:** 20h
- **Investigación sobre la tecnología Docker:** 6h
- **Investigación sobre el perfil de DevOps:** 2h
- **Generar ejemplos y comprobar su funcionamiento:** 15h
- **Interconectar los roles para NextCloud:** 5h
- **Redactado del proyecto :** 13h
- **Redactado del CookBook:** 3h

**TOTAL:** 64 horas.

![](images/tiempo-ejecucion.png)

# Resultados

Durante todo el trabajo se han ido generando múltiples ejemplos totalmente funcionales sobre la automatización de tareas en Ansible, estos ejemplos, se adjuntan con toda la documentación y además de un enlace a GitLab que almacenará dichos archivos permanentemente y de manera totalmente libre para todo aquel que quiera acceder a ellos.

Se ha generado también un CookBook (libro de recetas) con algunas tareas que no aparecen en los ejemplos. En este libro no se detalla el funcionamiento de las mismas, simplemente la manera de ejecutar dicha tarea, por ejemplo:

> **Crear un usuario:**
> 
> ```yaml
> -- name: Crear usuario
>    user:
>      name: Usuario_ejemplo
>      password: segura
> ```


Todo el documento ha sido redactado y generado íntegramente con la tecnología Markdown:

**MARKDOWN**

Markdown es un lenguaje de marcado ligero que simplifica mucho la generación de documentos.

Este lenguaje hace uso de marcadores sencillos como pueden ser los asteriscos para poner textos en negrita

|Markdown|Resultado| 
|---|---|
| `**Negrita**`  | **Negrita**  |
| `*Cursiva*`  | *Cursiva*  |
| `[textoURL](enlace)`  | [textoURL](enlace)  |

Este lenguaje está disponible en muchos programas, como por ejemplo:
 - Whatsapp
 - Telegram
 - Discord

Gracias a este marcado rápido, se puede exportar a casi cualquier formato, Word, HTML, PDF, EPUB...etc

Incluso se podría formatear a PDF con unas características específicas, como el tamaño de folio, la fuente por defecto, el tamaño de la fuente, un título, un índice enlazado con los títulos...

Se puede exportar a PDF de dos formas, una, simple con unos valores genéricos y sin posibilidad de editar prácticamente nada.

Para esto último es necesario utilizar una utilidad externa llamada [Pandoc](http://pandoc.org/) con el motor [LaTeX](https://www.latex-project.org/) e introducir una serie de cabeceras al principio del documento. Y después tan solo habría que exportarlo a PDF mediante consola con un comando:


Y otra que se basa en una utilidad llamada [Pandoc](http://pandoc.org), que a su vez, utiliza un motor llamado [LaTeX](https://www.latex-project.org)

**¿Qué es pandoc?**
Pandoc es un conversor de documentos, es libre y de código abierto. Su uso general es para convertir documentos de Markdown hacia otro tipo.

Es muy usado por académicos ya que el lenguaje Markdown ofrece mucha libertad y facilidad de uso a la hora de crear ciertos documentos científicos o matemáticos debido a su simplicidad de marcado.


> El formato de archivo mejor soportado por Pandoc es una versión extendida de Markdown, perotambién puede leer otros formatos como lenguajes de marcado ligeros, HTML, ReStructuredText, LaTeX, OPML, Org-mode, DocBook, y Office Open XML (Microsoft Word .docx). [Wikipedia](https://es.wikipedia.org/wiki/Pandoc#Formatos_de_archivo_soportados)

**¿Qué es LaTeX?**

LaTeX es un sistema de composición de archvios que no puede funcionar solo, si no que necesita de un conversor que haga sudo de él, ya que es solo un motor de conversión. Alguna de sus ventajas son:

- Consigue documentos que no pierden calidad al ser ampliados.
- Facilita la introducción de fórmulas y el uso de la notación matemática.
- Separa el estilo, del propio contenido.
- Permite dividir un proyecto en distintos ficheros, que son compilados para obtener el resultado final.
- Maneja bien la bibliografía, por defecto.

Es muy potente y por esa misma razón es el sistema que más se utiliza con pandoc.

Aunque también se puede utilizar sin pandoc, su nomenclatura es bastante compleja, por eso no tiene un público general, si no específico por científicos y académicos.

![](images/latex.png)


**Instalaciones:**
Al depender Pandoc del motor LaTeX, es necesario instalar los dos en el sistema para que la conversión funcione:

**Para instalar Pandoc:** (ubuntu 16.04):

`$ sudo apt install pandoc`

**Para instalar el motor LaTeX:** (ubuntu 16.04)

`$ sudo apt install texlive-xetex`

Para convertir un documento en markdown a pdf, por ejemplo, el comando a usar sería el siguiente:

`pandoc <archivo>.md --latex-engine=xelatex -o <destino>.pdf`



# Conclusiones

A lo largo del proyecto he aprendido como automatizar muchas de las tareas cotidianas y no tan cotidianas de un administrador de sistemas. Asimismo también he logrado adentrarme un poco dentro del pervil de DevOps, perfil que está muy demandado en el momento de la realización del proyecto.

Respecto a Ansible, es una tecnología muy capaz que pretende ahorrar muchísimo tiempo a los administradores de sistemas, aunque como todo, si las cosas se quieren hacer bien para que todo funcione en cualquier sistema, requiere de tiempo y dedicación a la hora de realizar los roles y los playbooks, pues no todos los servidores son iguales y puede haber mucha variación entre unos y otros, como puede ser el cambio del script de arranque de las aplicaciones de ubuntu 14.04 a ubuntu 16.04.

Aunque brevemente, se habló también de docker, una tecnología muy puntera ahora mismo tanto en el desarrollo de aplicaciones como en el mundo de sistemas. Hay que tenerlo muy en cuenta y mirarlo con detenimiento, pues se está popularizando muy rápido por todas sus ventajas.

Al final de todo el proceso, se percibe que el perfil de DevOps está en auge y que investigar todas las tecnologías de su entorno supondrá una clara ventaja a la hora de encontrar trabajo, puesto que facilitan mucho la vida al administrador de sistemas y aumentan mucho su productividad.


# Aclaraciones

**Aclaraciones sobre los hosts en línea de comandos:**

En los comandos de `ansible-playbook` se pone el `-i` para indicar el inventario.

El `-i` hace referencia a "inventario" y es de tipo lista, esto quiere decir que si nosotros ponemos:

 - `-i 192.168.1.2` estará **mal**, ya que lo que se le está diciendo a la opción es que use el inventario "192.168.1.2"

 - `-i 192.168.1.2,` estará **bien**, la diferencia está en la coma de detrás, al poner la coma, indicamos que es una lista, aunque no haya nada detrás, ansible lo tomará como una lista y entonces lo ejecutará hacia esa dirección.

 - `-i 192.168.1.2,192.168.1.3` esto, aunque no tiene coma final, está **bien**, ya que con la primera coma ya le hemos indicado que es una lista.

**Aclaraciones sobre los módulos:**

Los módulos utilizados, se han usado solo con las opciones que se ha necesitado, pero tienen más opciones disponibles en la documentación oficial de ansible:

Por ejemplo, el módulo usuario: [Módulo usuario](http://docs.ansible.com/ansible/user_module.html)

**Tienen 4 descripciones:**
	- **parameter:** el parámetro a utilizar, por ejemplo `name`
	- **required:** si es requerido o no para que funcione
	- **default:** opción que tomará por defecto si no es requerido y no lo ponemos
	- **choices:** las opciones que puede incluir ese campo en concreto

**¿Son necesarios tantos archivos?**	

En los ejemplos se ha intentado modular lo máximo posible para que quedara todo claro respecto a la estructura, pero como ya se puso en los apartados anteriores, podría hacerse todo incluso dentro de un solo archivo `playbook.yml`. 

Aunque no es lo ideal, ya que la filosofía de ansible es agilizar el trabajo, si realizas todo en un mismo playbook, no podrás reutilizar ese playbook si el próximo cliente donde lo tengas que usar no es exactamente idéntico.

Hay que modular y separar por roles cada parte de la instalación para poder luego ajustar cada acción a otra instalación.


# Anexos

Todos los archivos y documentación se pueden encontrar en un repositorio que habilité para que los archivos estuvieran siempre disponibles de forma libre para todo el mundo.

El acceso al repositorio es público (no requiere ni cuenta en GitLab, ni un permiso especial.

Enlace al repositorio: [https://goo.gl/MEVZbq](https://goo.gl/MEVZbq)
Imagen QR que enlaza directamente al repositorio (mismo enlace que el de arriba)

![](images/qr-gitlab-mini.jpg)


# Bibliografía y Webgrafía
**Ansible:**
[http://docs.ansible.com/ansible/intro_installation.html](http://docs.ansible.com/ansible/intro_installation.html)

[http://www.cloudadmins.org/2014/04/ansible-automatizacion-de-tareas-y-despliegues-de-forma-simple/](http://www.cloudadmins.org/2014/04/ansible-automatizacion-de-tareas-y-despliegues-de-forma-simple/)

**Información sobre los módulos:**
[http://docs.ansible.com/ansible/modules.html](http://docs.ansible.com/ansible/modules.html)

**Docker:**
[https://openwebinars.net/blog/docker-que-es-sus-principales-caracteristicas/](https://openwebinars.net/blog/docker-que-es-sus-principales-caracteristicas/)

[http://www.javiergarzas.com/2015/07/que-es-docker-sencillo.html](http://www.javiergarzas.com/2015/07/que-es-docker-sencillo.html)

**DevOps:**
[https://www.xeridia.com/blog/sabes-realmente-que-es-devops](https://www.xeridia.com/blog/sabes-realmente-que-es-devops)

[http://www.hi-techip.com/devops-te-lo-cuento/](http://www.hi-techip.com/devops-te-lo-cuento/)

**Gráfico generado con:** 
[https://www.meta-chart.com](https://www.meta-chart.com)

**Pandoc y LaTeX:**
[https://es.wikipedia.org/wiki/Pandoc#Formatos_de_archivo_soportados](https://es.wikipedia.org/wiki/Pandoc#Formatos_de_archivo_soportados)

[https://www.somosbinarios.es/que-es-latex-como-usar-latex/](https://www.somosbinarios.es/que-es-latex-como-usar-latex/)

**Imágenes:**

`ansible.png`:[https://www.ansible.com/logos](https://www.ansible.com/logos)

`esquema1.png`: [https://sysadmincasts.com/episodes/46-configuration-management-with-ansible-part-3-4](https://sysadmincasts.com/episodes/46-configuration-management-with-ansible-part-3-4)

`esquema2.png`:[http://slides.com/racku/ansible-fundamentals#/](http://slides.com/racku/ansible-fundamentals#/)

`devops.png`: [https://www.xeridia.com/blog/sabes-realmente-que-es-devops](https://www.xeridia.com/blog/sabes-realmente-que-es-devops)

`docker.png`:[https://www.docker.com/company/news-and-press](https://www.docker.com/company/news-and-press)

`docker1.png`:[https://blog.jayway.com/2015/03/21/a-not-very-short-introduction-to-docker/](https://blog.jayway.com/2015/03/21/a-not-very-short-introduction-to-docker/)

`docker2.jpg`: [https://moisesvilar.github.io/post/docker-layers-2/](https://moisesvilar.github.io/post/docker-layers-2/)
