#CookBook para Ansible.

Este es un pequeño documento que contiene una serie de códigos con los que complementar el documento del trabajo, no se tratarán asuntos ya vistos en la documentación.

## Usuarios:

**Borrar un usuario:**

```yaml
- name: borrar usuario
  user:
    name: pepito
    state: absent
``` 

**Crear pareja de claves:**

```yaml
- name: crear pareja de claves
  user:
    name: pepito
    generate_ssh_key: yes
    ssh_key_bits: 2048 #no obligatorio
    ssh_key_file: .ssh/id_rsa #no obligatorio
``` 

## Archivos:

**Descomprimir un archivo local en los clientes:**

```yaml
- name: Descomprimir algo.zip
  unarchive:
    src: algo.zip
    dest: /tmp/algo/  
``` 

**Descomprimir un archivo del cliente en él mismo:**

```yaml
- name: Descomprimir algo.zip
  unarchive:
    src: algo.zip
    dest: /tmp/algo/ 
    remote_src: yes 
``` 

**Descargar un archivo de internet en el cliente:**

```yaml
- name: Descargar algo.zip
  get_url:
    src: albertomerlo.com/algo.zip
    dest: /tmp/algo/algo.zip #si pones pulpo.zip le cambia el nombre 
    owner: pepito # dueño del archivo
    group: administradores # grupo del archivo
``` 

**Comprimir una carpeta:**

```yaml
- name: Comprimir fotos
  archive:
    path: /home/pepito/fotos
    dest: /home/pepito/fotos.tgz
```


## Servicios:

**Reiniciar un servicio:**

```yaml
- name: Reinicar apache2
  service:
  name: apache2
  state: restarted
```

**Parar un servicio:**

```yaml
- name: parar apache2
  service:
  name: apache2
  state: stopped
```

**Iniciar un servicio:**

```yaml
- name: Iniciar apache2
  service:
  name: apache2
  state: started
```

## Administrar firewall UFW

**Activar UFW:**

```yaml
- name: Activar UFW
  ufw:
    state: enable
```

**Desactivar UFW:**

```yaml
- name: Desactivar UFW
  ufw:
    state: disabled
```

**Denegar todo por defecto:**

```yaml
- name: Denegar todo por defecto
  ufw:
    policy: deny
```

**Aceptar todo por defecto:**

```yaml
- name: Aceptar todo por defecto
  ufw:
    policy: allow
```

**Abrir un puerto:**

```yaml
- name: Abrir puerto 22 tcp
  ufw:
    rule: allow
    port: 22
    proto: tcp # o udp, si no se pone "proto", se abren ambos
```


**Cerrar un puerto:**

```yaml
- name: Cerrar puerto 22 tcp/udp
  ufw:
    rule: deny
    port: 22
```

**Abrir un rango de puertos a una IP:**

```yaml
- name: Abir a 192.168.1.32 los puertos del 10 al 20
  ufw:
    rule: allow
    port: 10:20
    src: 192.168.1.32
```